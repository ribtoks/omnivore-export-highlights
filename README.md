# Export highlights in markdown from Omnivore

## About

The purpose of this script is to export all highlights from Omnivore in the markdown format. This enables their import into other note-taking apps like Joplin. This script is possible because of the [OmnivoreQL](https://github.com/yazdipour/OmnivoreQL) python wrapper over GraphQL API of the Omnivore.

## Usage

- create a virtualenv for this script (e.g. `mkvirtualenv omnivore`)
- install dependencies using `pip install -r requirements.txt`)
- create an API key in Omnivore
- run script using `OMNIVORE_API_TOKEN=xyz python export.py`