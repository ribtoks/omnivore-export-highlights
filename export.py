import argparse
import os
import os.path
import logging
import sys
from omnivoreql import OmnivoreQL


class ArticleHighlights:
    def __init__(self, url, title):
        self.url = url
        self.title = title
        self.highlights = []
        self.created_at = None

    def add_highlights(self, highlights):
        for h in highlights:
            self.created_at = h["createdAt"]
            self.highlights.append(h["quote"])

    def save(self, dir, slug):
        fname = f"{dir}/{slug}.md"
        if os.path.isfile(fname):
            logging.debug("Skipping existing file %s", fname)
            return

        body = [
            f"# {self.title}",
            "",
            f"[Source]({self.url})",
            "",
            f"{len(self.highlights)} highlights",
            "",
            f"Created at {self.created_at}",
            "",
            "---",
            "",
        ]
        dups = set()
        for h in self.highlights:
            if h in dups:
                continue
            dups.add(h)
            body.append(f"* {h}")
            body.append("")

        with open(fname, "w") as text_file:
            text_file.write("\n".join(body))


def main():
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("gql.transport.requests").setLevel(logging.WARNING)

    parser = argparse.ArgumentParser("export-highlights")
    parser.add_argument(
        "--export-dir",
        type=str,
        default="export",
        help="Path to export directory",
    )

    args = parser.parse_args(sys.argv[1:])
    os.makedirs(args.export_dir, exist_ok=True)

    omnivoreql_client = OmnivoreQL(os.environ["OMNIVORE_API_TOKEN"])

    profile = omnivoreql_client.get_profile()

    has_next_page = True
    after = 0

    slug_to_highlights = {}

    while has_next_page:
        articles = omnivoreql_client.get_articles(
            query="in:all has:highlights mode:highlights", limit=1, after=after
        )["search"]

        pageInfo = articles["pageInfo"]
        logging.info(
            "Fetched highlights. startCursor=%s endCursor=%s count=%s",
            pageInfo["startCursor"],
            pageInfo["endCursor"],
            pageInfo["totalCount"],
        )

        for edge in articles["edges"]:
            article = edge["node"]
            slug = article["slug"]
            title = article["title"]
            url = article["originalArticleUrl"]

            article_highlights = None

            if not slug in slug_to_highlights:
                article_highlights = ArticleHighlights(url, title)
                slug_to_highlights[slug] = article_highlights
            else:
                article_highlights = slug_to_highlights[slug]

            article_highlights.add_highlights(article["highlights"])

            has_next_page = pageInfo["hasNextPage"]
            after = int(pageInfo["endCursor"])

    logging.info("Processed articles. count=%s", len(slug_to_highlights))

    for slug in slug_to_highlights:
        slug_to_highlights[slug].save(args.export_dir, slug)


if __name__ == "__main__":
    main()
